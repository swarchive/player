# player

Shockwave Player Archive


# Contributing

Have a version that is not currently included here? Please open an issue on this repo providing a copy.


# Info

Every file is organized by version, build, and target, as best as possible.

Some versions have multiple builds of the same version.

Subsequent builds get an incremental number added to the version (ie. `1.2.3.4`, `1.2.3.4-2`, `1.2.3.4-3`).

Additional builds are not always built for every target.

Older versions often have unclear or conflicting version info, a best guess was made based on:

- Code signatures
- Installer logs
- Registry and preference data
- Binary resource version info
- URL of the file

Each file has a `sources.txt` file documenting places where the file was originally obtained.

The correct original file name is preserved as best as possible and practicle.

Some copies where obtained from third party mirrors, but were checked for authenticity.

Shim installers and ActiveX codebases would attempt to download additional software, which is likely unavailable.

Some old shim installers are Netscape only.

Shock installers do not include the browser plugins, just the runtime for Shockwave projectors.

Some old full installers may not include the Netscape plugin from the shim installer.

Slim installers downloads Xtras as needed, which are likely unavailable.

The `lic-msi` installers include the Windows-only Shockwave 10 compatability layer that would otherwise downloaded as needed, which might be unavailable.

Every version in the archives is available outside of an archive.


# Glossary

A glossary of target tokens:

- `win`: Windows
- `mac`: Mac
- `shim`: A shim installer that would download the other parts from online
- `shock`: The core of Shockwave without the browser plugins
- `full`: A full installer, older versions may not include NPAPI plugin found in shim version
- `slim`: Similar to full installer, but some Xtras are automatically downloaded as needed
- `ns`: Netscape only
- `activex`: ActiveX codebase version of shim
- `archive`: An archive containing installers
- `msi`: MSI installer
- `lic`: "Licensed" installer
- `preview`: A preview build
- `foo`: An installer that would be downloaded by a shim to finish installation

Additionally some targets have OS, processor, and language information.


# Issues

If you believe you have found a mistake in the versioning of the software (note that some old versions have conflicting or even wrong version information), have a new version available, or noticed a new version has been released, please open an issue on this repo.

Please note the software in this repo is provided as-is, and technical support is not provided.
